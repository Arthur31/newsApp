import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home/home.component';
import { NewsComponent } from './news/news.component';
import { SNewsComponent } from './s-news/s-news.component';
import { NotExistComponent } from './not-exist/not-exist.component';


const routes: Routes = [
  {
    path : '',
    redirectTo : 'home',
    pathMatch : 'full'
  },
  {
    path : 'home',
    component : HomeComponent
  },
  {
    path : 'news',
    component : NewsComponent
  },
  {
    path : 'news/:id',
    component : SNewsComponent
  },
  {
    path : '**',
    component : NotExistComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
