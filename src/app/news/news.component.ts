import { Component, OnInit } from '@angular/core';

import { StorageService } from '../storage.service';


@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.scss']
})
export class NewsComponent implements OnInit {
  articles:any;
  constructor(private storageService: StorageService) {
  }

  ngOnInit() {
    this.articles = this.storageService.getNews();
  }

}
