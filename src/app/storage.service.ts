import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class StorageService {
private allNews = [
  {
    id : 'news-0001',
    title : "Hello World",
    description : "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
    image : "http://alfoldiregiomagazin.hu/wp-content/uploads/2018/04/robot-computer.jpg"
  },
  {
    id : 'news-0002',
    title : "Elon Musk n’aime vraiment pas les motos et Tesla n’en aura pas",
    description : "Parole d’Elon Musk, Tesla ne construira pas de motos de sitôt. Et la raison est plutôt personnelle.\
  Dans une rencontre d’actionnaires tenue hier, un membre de l'auditoire a demandé à Musk, PDG de Tesla, si le constructeur de voitures électriques pourrait un jour se lancer dans le développement d’une moto.\
  Elon Musk a rapidement répondu par la négative, expliquant qu’il a lui-même déjà conduit une moto dans sa jeunesse. Toutefois, à l’âge de 17 ans, il a failli se faire frapper par un camion et a décidé de mettre un terme à sa carrière de motard.\
  Quoiqu’en pense Musk, l’idée d’une moto électrique est loin d’être farfelue. L’entreprise Zero Motorcycles en a fait sa spécialité et même de grandes marques comme Yamaha et Harley-Davidson travaillent en ce sens.",
    image : "https://static-news.moneycontrol.com/static-mcnews/2017/07/RTX3C49K-770x433.jpg"
  },
  {
    id : 'news-0003',
    title : "Facebook va financer des programmes d'information sur sa plate-forme Watch",
    description : "Mois après mois, Facebook dévoile les pans de sa stratégie destinée à lutter contre la propagation de fausses informations sur sa plate-forme, annoncée en début d'année par son PDG fondateur, Mark Zuckerberg.\
A partir de cet été, le réseau social va financer des programmes d'information produits par différents médias américains et qui seront diffusés exclusivement sur sa  plate-forme dédiée aux contenus vidéos, Watch . « Avec cette initiative, nous testons un espace réservé à l'information précise et de qualité », présente la responsable des partenariats journalistiques Campbell Brown  dans un post .",
    image : "https://www.lesechos.fr/medias/2018/06/07/2182115_facebook-va-financer-des-programmes-dinformation-sur-sa-plate-forme-watch-web-tete-0301776449618.jpg"
  },
  {
    id : 'news-0004',
    title : "Prix cassés, ristournes cachées : les petits arrangements de la campagne d’Emmanuel Macron",
    description : "Des entreprises ont bien cassé les prix pour se mettre au service d’Emmanuel Macron pendant la campagne présidentielle. Mediapart et Le Monde ont déjà révélé que certaines remises accordées au candidat d’En Marche ont attiré l’attention de la commission nationale des comptes de campagne (CNCCFP), qui, après examen, a  finalement jugé que ces ristournes étaient \"normales et régulières\". Cependant, l’enquête de la cellule investigation de Radio France montre que ce ne sont pas les seules conditions avantageuses dont a bénéficié Emmanuel Macron. Certains rabais conséquents ont échappé à la vigilance de l’autorité de contrôle. \
    Jean-Marc Dumontet : des théâtres à prix d’ami\
    <span>Drôles d’endroits pour une campagne présidentielle. A deux reprises, Emmanuel Macron est monté sur la scène de théâtres parisiens, le 6 février 2017 à Bobino, puis le 8 mars au Théâtre Antoine. Point commun des deux salles : leur propriétaire, Jean-Marc Dumontet (il est copropriétaire du Théâtre Antoine avec Laurent Ruquier). En quelques années, il a constitué un petit empire dans le secteur, JMD Prod. L’homme qui a lancé Nicolas Canteloup notamment est aujourd’hui un personnage incontournable du théâtre français. Il présidait le 28 mai dernier la cérémonie des Molières.\
    Lors de la campagne présidentielle, Jean-Marc Dumontet est devenu proche du couple Macron. Il s’est engagé publiquement en faveur de l’ancien ministre de l’Economie qu’il a alimenté en notes et en conseils. Le 8 mars 2017, à la fin d’une soirée consacrée à l’égalité hommes-femmes, Brigitte Macron le remercie chaleureusement d’avoir accueilli ce meeting de campagne.",
    image : "https://cdn.radiofrance.fr/s3/cruiser-production/2018/06/904754f2-9283-4de7-9b65-621618c6f16e/640_000_o09n2.jpg"
  }
]
  constructor() { }
  returnA:any;

  getNews () {
    return this.allNews;
  }
  getNewsById (id:string) {
    return this.allNews.find(news => {
      return news.id === id;
    });

  }

  getLastNews(){
    this.returnA = [];
    this.returnA = this.allNews;
    return this.returnA.reverse().slice(0,3);
  }
}
