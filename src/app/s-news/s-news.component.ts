import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { StorageService } from '../storage.service';

@Component({
  selector: 'app-s-news',
  templateUrl: './s-news.component.html',
  styleUrls: ['./s-news.component.scss']
})
export class SNewsComponent implements OnInit {
  article : any;
  newsId : any;
  constructor(private route : ActivatedRoute, private storageService: StorageService) {

  }

  ngOnInit() {
    this.newsId = this.route.snapshot.params['id'];
    this.article = this.storageService.getNewsById(this.newsId);
  }

}
