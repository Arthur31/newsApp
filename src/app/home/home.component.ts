import { Component, OnInit } from '@angular/core';
import { StorageService } from '../storage.service';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  articles:any;

  constructor(private storageService: StorageService) {
  }

  ngOnInit() {
    this.articles = this.storageService.getLastNews();
  }

}
